# coding: utf-8
from django.contrib import admin

from keywords.models import KeyWordSet


admin.site.register(KeyWordSet)
