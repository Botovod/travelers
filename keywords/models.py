# coding: utf-8
from __future__ import unicode_literals
from django.db import models


class KeyWordSet(models.Model):
    keywords = models.CharField('Ключевые слова', max_length=255, default='', null=False, blank=False)
    return_value = models.CharField('Возвращаемое значение', max_length=255, default='', null=False, blank=False)

    def __unicode__(self):
        return self.keywords

    class Meta:
        verbose_name = 'Набор ключевых слов'
        verbose_name_plural = 'Наборы ключевых слов'



