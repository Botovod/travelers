# coding: utf-8
from __future__ import unicode_literals
from keywords.models import KeyWordSet


def analyze(input_string):
    return_value = []
    for set in KeyWordSet.objects.all():
        for word in set.keywords.split(','):
            if word.strip() in input_string:
                return_value += word.strip()

    return return_value
