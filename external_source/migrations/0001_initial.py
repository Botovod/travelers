# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0048_auto_20161220_2210'),
    ]

    operations = [
        migrations.CreateModel(
            name='CityExternalSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.CharField(default='', max_length=255, verbose_name='URL', blank=True)),
                ('city', models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', to='travelers.City')),
            ],
            options={
                'verbose_name': '\u0412\u043d\u0435\u0448\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441 \u0433\u043e\u0440\u043e\u0434\u0430',
                'verbose_name_plural': '\u0412\u043d\u0435\u0448\u043d\u0438\u0435 \u0440\u0435\u0441\u0443\u0440\u0441\u044b \u0433\u043e\u0440\u043e\u0434\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SightExternalSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.CharField(default='', max_length=255, verbose_name='URL', blank=True)),
                ('sight', models.ForeignKey(verbose_name='\u0414\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c', to='travelers.Sight')),
            ],
            options={
                'verbose_name': '\u0412\u043d\u0435\u0448\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441 \u0434\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043e\u0441\u0442\u0438',
                'verbose_name_plural': '\u0412\u043d\u0435\u0448\u043d\u0438\u0435 \u0440\u0435\u0441\u0443\u0440\u0441\u044b \u0434\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043e\u0441\u0442\u0435\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SourceSite',
            fields=[
                ('slug', models.SlugField(max_length=255, serialize=False, verbose_name='\u041a\u043b\u044e\u0447', primary_key=True)),
                ('base_url', models.CharField(default='', max_length=255, verbose_name='\u0411\u0430\u0437\u043e\u0432\u044b\u0439 URL')),
            ],
            options={
                'verbose_name': '\u0421\u0430\u0439\u0442 \u0438\u0441\u0442\u043e\u0447\u043d\u0438\u043a',
                'verbose_name_plural': '\u0421\u0430\u0439\u0442\u044b \u0438\u0441\u0442\u043e\u0447\u043d\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='sightexternalsource',
            name='source_site',
            field=models.ForeignKey(verbose_name='\u0421\u0430\u0439\u0442', to='external_source.SourceSite'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cityexternalsource',
            name='source_site',
            field=models.ForeignKey(verbose_name='\u0421\u0430\u0439\u0442', to='external_source.SourceSite'),
            preserve_default=True,
        ),
    ]
