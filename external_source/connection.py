# coding: utf-8
from __future__ import unicode_literals
from external_source.models import SourceSite
from grab import Grab
import time


def create_connection(base_url, url):
    page = Grab()
    connection_success = False
    while not connection_success:
        try:
            page.go(base_url + url)
            print 'Try to connect to ' + base_url + url
        except Exception as e:
            print e
            time.sleep(2)
            print 'Reconnection'
        else:
            connection_success = True
            print "connected"
    return page


def create_site_connection_by_slug(slug, url):
    return create_connection(SourceSite.objects.get(slug=slug).base_url, url)
