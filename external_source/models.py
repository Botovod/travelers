# coding: utf-8
from __future__ import unicode_literals
from django.db import models
from travelers.models import Sight, City


class SourceSite(models.Model):
    slug = models.SlugField('Ключ', max_length=255, blank=False, db_index=True, primary_key=True)
    base_url = models.CharField('Базовый URL', max_length=255, default='', blank=False)

    class Meta:
        verbose_name = "Сайт источник"
        verbose_name_plural = "Сайты источники"


class ExternalSource(models.Model):
    link = models.CharField('URL', max_length=255, default='', blank=True)
    source_site = models.ForeignKey(SourceSite, verbose_name='Сайт', blank=False, null=False)

    class Meta:
        abstract = True
        verbose_name = "Внешний ресурс"
        verbose_name_plural = "Внешние ресурсы"


class SightExternalSource(ExternalSource):
    sight = models.ForeignKey(Sight, verbose_name='Достопримечательность', blank=False, null=False)

    class Meta:
        verbose_name = "Внешний ресурс достопримечательости"
        verbose_name_plural = "Внешние ресурсы достопримечательостей"


class CityExternalSource(ExternalSource):
    city = models.ForeignKey(City, verbose_name='Город', blank=False, null=False)

    class Meta:
        verbose_name = "Внешний ресурс города"
        verbose_name_plural = "Внешние ресурсы городов"
