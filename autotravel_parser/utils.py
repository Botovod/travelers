# coding: utf-8
from __future__ import unicode_literals
from travelers.models import Sight, City
from external_source.models import SourceSite
from slugify import slugify
from external_source.connection import create_connection


def coordinates_to_str(coordinates):
    # for gradus-minute format
    str_coordinates = ''
    try:
        coordinate1 = coordinates.split(',')[0].strip()
        coordinate2 = coordinates.split(',')[1].strip()
        if coordinate1[0] in ['N', 'n']:
            coordinate1 = coordinate1[1:].strip('0')
        else:
            coordinate1 = '-' + coordinate1[1:].strip('0')

        if coordinate2[0] in ['E', 'e']:
            coordinate2 = coordinate2[1:].strip('0')
        else:
            coordinate2 = '-' + coordinate2[1:].strip('0')

        coordinate1_gradus_part = coordinate1.split(' ')[0]
        coordinate1_minutes_part = float(coordinate1.split(' ')[1])
        coordinate2_gradus_part = coordinate2.split(' ')[0]
        coordinate2_minutes_part = float(coordinate2.split(' ')[1])
        coordinate1 = str(float(coordinate1_gradus_part) + coordinate1_minutes_part / 60)
        coordinate2 = str(float(coordinate2_gradus_part) + coordinate2_minutes_part / 60)

    except IndexError:
        pass
    else:
        str_coordinates = coordinate1 + ', ' + coordinate2
    return str_coordinates


def create_sight_from_parsed_data(title, coordinates, description, city, sight_url):
    try:
        new_sight, created = Sight.objects.get_or_create(title=title, city=city)
    except Sight.MultipleObjectsReturned:
        print "Задвоенные досты"
        for dubled_sight in Sight.objects.filter(title=title, city=city):
            if dubled_sight.sighphoto_set.all().count() == 0:
                dubled_sight.delete()
        try:
            new_sight, created = Sight.objects.get_or_create(title=title, city=city)
        except Sight.MultipleObjectsReturned:
            print "Не сработал вариант 1"
            new_sight = Sight.objects.filter(title=title, city=city)[0]
            created = False
            for need_to_delete_sight in Sight.objects.filter(title=title, city=city)[1:]:
                need_to_delete_sight.delete()

    if created:
        print "new sight found"
    str_coordinates = coordinates_to_str(coordinates.strip())
    posted = False
    need_to_save = False
    if str_coordinates:
        posted = True
    if new_sight.original_coordinates != coordinates:
        new_sight.original_coordinates = coordinates
        need_to_save = True
    if new_sight.coordinates != str_coordinates:
        new_sight.coordinates = str_coordinates
        need_to_save = True
    if new_sight.text != description:
        print new_sight.text + " changed to " + description
        new_sight.text = description
        need_to_save = True
    if new_sight.posted != posted:
        new_sight.posted = posted
        need_to_save = True
    if new_sight.autotravel_url != sight_url:
        new_sight.autotravel_url = sight_url
        need_to_save = True
    if need_to_save:
        new_sight.save()
    return new_sight


def create_city_from_parsed_data(title, region, posted, regional_center, town_url):
    new_city, created = City.objects.get_or_create(title=title, region=region)
    if created:
        print "new city found"
    need_to_save = False
    if new_city.slug == '':
        new_city.slug = slugify(title)
        need_to_save = True
    # if new_city.posted != posted:
    #     new_city.posted = posted
    #     need_to_save = True
    # if new_city.regional_center != regional_center:
    #     new_city.regional_center = regional_center
    #     need_to_save = True
    if new_city.autotravel_url != town_url:
        print new_city.autotravel_url + " changed to " + town_url
        new_city.autotravel_url = town_url
        need_to_save = True
    if need_to_save:
        new_city.save()
        print "city resaved"
    return new_city


def create_att_connection(att_url):
    return create_connection(SourceSite.objects.get(slug='autotravel').base_url, att_url)


def parse_sight_page(sight):
    if sight.autotravel_url:
        sight_page = create_att_connection(sight.autotravel_url)
        sight_title = sight_page.doc.tree.xpath('//h1/text()')[0]
        print sight_title
        if sight.title == sight_title:
            sight_coodinates = ''
            sight_description = ''
            try:
                sight_coodinates = sight_page.doc.tree.xpath('//font[@class="travell2"]/text()')[0].strip()
                sight_description = sight_page.doc.tree.xpath('//p[@class="travell0u"]')[0]
            except IndexError:
                pass
            sight_description_text = sight_description.text_content().strip()
            str_coordinates = coordinates_to_str(sight_coodinates.strip())
            # print sight_description_text
            # print str_coordinates
            # print sight_coodinates
            print '---'
            posted = False
            need_to_save = False
            if str_coordinates:
                posted = True
            if sight.original_coordinates != sight_coodinates:
                sight.original_coordinates = sight_coodinates
                need_to_save = True
            if sight.coordinates != str_coordinates:
                sight.coordinates = str_coordinates
                need_to_save = True
            if sight.text != sight_description_text:
                sight.text = sight_description_text
                print sight.text + " changed to " + sight_description_text
                need_to_save = True
            if sight.posted != posted:
                sight.posted = posted
                need_to_save = True
            if need_to_save:
                print "saved"
                sight.save()
        else:
            print 'Wrong title'
    return sight


def check_and_assign_fields(object1_field, object2_field):
    if not object1_field:
        object1_field = object2_field


def merge_cities(city1, city2):
    check_and_assign_fields(city1.region, city2.region)
    check_and_assign_fields(city1.regional_center, city2.regional_center)
    check_and_assign_fields(city1.show_on_mainmap, city2.show_on_mainmap)
    check_and_assign_fields(city1.posted, city2.posted)
    check_and_assign_fields(city1.centre_coordinates, city2.centre_coordinates)
    check_and_assign_fields(city1.default_zoom, city2.default_zoom)
    check_and_assign_fields(city1.description, city2.description)
    city2.save()
    city2.sight_set.all().update(city=city1)
    return city1
