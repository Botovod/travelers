# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from travelers.models import Sight, City
from autotravel_parser.utils import merge_cities


class Command(BaseCommand):
    def handle(self, *args, **options):
        for city in City.objects.filter(regional_center=True):
            for other_city in City.objects.filter(title=city.title).exclude(id=city.id):
                if other_city.id > city.id:
                    merge_cities(city, other_city)
                    other_city.delete()
                else:
                    merge_cities(other_city, city)
                    city.delete()
