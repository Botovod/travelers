# coding: utf-8
from __future__ import unicode_literals
from django.core.management.base import NoArgsCommand
from django.conf import settings
import os
from autotravel_parser.utils import create_city_from_parsed_data, create_sight_from_parsed_data, create_att_connection
from travelers.models import Region, SighPhoto


class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        towns_page = create_att_connection('/towns.php')

        for towns_url in towns_page.doc.tree.xpath('//center/table/tbody/tr[5]/td/b/a/@href'):
            towns_by_letter_page = create_att_connection(towns_url)

            for column_number in range(1 if settings.PARSER_DEBUG else 2):
                for town_number in range(1 if settings.PARSER_DEBUG else len(towns_by_letter_page.doc.tree.xpath('//tr[1]/td[{}]/a[@class="travell5"or@class="travell5c"or@class="travell5s"]'.format(column_number + 1)))):
                    town_name = towns_by_letter_page.doc.tree.xpath('//tr[1]/td[{}]/a[@class="travell5"or@class="travell5c"or@class="travell5s"][{}]/text()'.format(column_number + 1, town_number + 1))[0]
                    town_class = towns_by_letter_page.doc.tree.xpath('//tr[1]/td[{}]/a[@class="travell5"or@class="travell5c"or@class="travell5s"][{}]/@class'.format(column_number + 1, town_number + 1))[0]
                    town_url = towns_by_letter_page.doc.tree.xpath('//tr[1]/td[{}]/a[@class="travell5"or@class="travell5c"or@class="travell5s"][{}]/@href'.format(column_number + 1, town_number + 1))[0]
                    region_name = towns_by_letter_page.doc.tree.xpath('//tr[1]/td[{}]/font[@class="travell0"][{}]/text()'.format(column_number + 1, town_number + 1))[0]

                    new_region, region_created = Region.objects.get_or_create(title=region_name.strip().strip(')').strip('('))
                    if region_created:
                        print "New region created"
                    regional_center = False
                    new_city = create_city_from_parsed_data(town_name.strip(), new_region, regional_center,
                                                            regional_center, town_url)

                    sights_of_town = create_att_connection('/excite.php/' + town_url.split('/')[2] + '/1')

                    for sight_number in range(len(sights_of_town.doc.tree.xpath('//table[1]/tbody/tr/td[1]/b/a'))):
                        sight_title = sights_of_town.doc.tree.xpath('//table[1]/tbody/tr[{}]/td[1]/b/a/text()'.format(sight_number + 2))[0]
                        sight_url = sights_of_town.doc.tree.xpath('//table[1]/tbody/tr[{}]/td[1]/b/a/@href'.format(sight_number + 2))[0]
                        sight_coodinates = ''
                        try:
                            sight_coodinates = sights_of_town.doc.tree.xpath('//table[1]/tbody/tr[{}]/td[1]/text()'.format(sight_number + 2))[0].strip()
                        except IndexError:
                            pass

                        sight_description_text = ''
                        try:
                            sight_description = sights_of_town.doc.tree.xpath('//table[1]/tbody/tr[{}]/td[2]'.format(sight_number + 2))[0]
                            sight_description_text = sight_description.text_content()
                            sight_description_text = sight_description_text.strip().strip("Прочитать последние несколько откликов:")
                        except IndexError:
                            pass

                        new_sight = create_sight_from_parsed_data(sight_title, sight_coodinates, sight_description_text,
                                                                  new_city, sight_url)
                        sight_page = create_att_connection(sight_url)

                        for img_url in sight_page.doc.tree.xpath('//td[2]/div[4]/a/@href'):
                            img_file = create_att_connection(img_url)
                            picture_path = os.path.join('pictures', new_city.slug, str(new_sight.id))
                            picture_filename = img_url.split('/')[-1]
                            save_path = os.path.join(settings.MEDIA_ROOT, picture_path, picture_filename)
                            if not os.path.exists(save_path):
                                img_file.doc.save(save_path)
                                print 'image saved'
                            try:
                                new_sight_photo, photo_created = SighPhoto.objects.get_or_create(
                                    file=os.path.join(picture_path, picture_filename),
                                    sight=new_sight)
                            except SighPhoto.MultipleObjectsReturned:
                                photo_created = False
                                for need_to_delete_sight in SighPhoto.objects.filter(file=os.path.join(picture_path, picture_filename), sight=new_sight)[1:]:
                                    need_to_delete_sight.delete()
                            if photo_created:
                                print "Resaved photo object"
