# coding: utf-8
from __future__ import unicode_literals


def clean_coordinates(str_coordinates):
    result_str = ''
    for part in str_coordinates:
        result_str += part.strip().strip('Широта:').strip('Долгота:').strip('°').strip('0').strip()
        if result_str and len(result_str.split(',')) == 1:
            result_str += ', '
    return result_str
