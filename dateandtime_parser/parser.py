# coding: utf-8
from __future__ import unicode_literals
from external_source.connection import create_site_connection_by_slug
from dateandtime_parser.utils import clean_coordinates


def find_in_table_column(cities_on_page, city_region_title):
    coordinates_text = ''
    for city_page_link in cities_on_page:
        coordinates_page = '/ru/citycoordinates.php?' + city_page_link.split('?')[1]
        city_page = create_site_connection_by_slug('dateandtime', coordinates_page)
        region_title = city_page.doc.tree.xpath(".//*[@id='west_side']/article/div[2]/h1/text()")[0].split(',')[1].strip()
        print region_title
        if region_title == city_region_title:
            print 'needed town'
            coordinates_text = city_page.doc.tree.xpath(".//*[@id='west_side']/article/div[2]/div[3]/text()")
            coordinates_text = clean_coordinates(coordinates_text)
            break
        print 'wrong town'
    return coordinates_text


def dateandtime_parse(cities):
    main_page = create_site_connection_by_slug('dateandtime', '/ru/country.php?code=RU&limit=0')
    for city in cities:
        cities_on_page = main_page.doc.tree.xpath(".//*[@id='city_table']/tbody/tr/td[1]/a[text() = '" + city.title + "']/@href")
        coordinates = find_in_table_column(cities_on_page, city.region.title)
        if not coordinates:
            cities_on_page = main_page.doc.tree.xpath(".//*[@id='city_table']/tbody/tr/td[3]/a[text() = '" + city.title + "']/@href")
            coordinates = find_in_table_column(cities_on_page, city.region.title)
        if not coordinates:
            print 'not work'
            break
        city.centre_coordinates = coordinates
        city.save()

