#!/bin/bash
export PYTHONPATH=`pwd`:$PYTHONPATH
python manage.py reset_db
python manage.py migrate
python manage.py loaddata first_cities
python manage.py loaddata cities
python manage.py loaddata type_of_sights
python manage.py loaddata section_of_sights
python manage.py loaddata perm_sight
python manage.py loaddata penza
python manage.py loaddata trace
