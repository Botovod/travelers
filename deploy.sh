#!/bin/bash
sudo supervisorctl stop russiantravel_gunicorn
git pull
../env/bin/python manage.py reset_db
psql -h localhost -U postgres -W travelers < travelers.sql
sudo supervisorctl start russiantravel_gunicorn
../env/bin/python manage.py collectstatic
