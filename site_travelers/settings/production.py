from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'travelers',
        'USER': 'postgres',
        'PASSWORD': '546626',
        'HOST': '127.0.0.1',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '5432',  # Set to empty string for default.
    }
}

MEDIA_ROOT = os.path.join('/', 'home', 'antoha', 'sites', 'russiantravel', 'www', 'media')

ALLOWED_HOSTS = ['*.fvds.ru', 'russiantravel.su', '82.146.48.190', 'localhost', '127.0.0.1']
