#coding: utf-8
from __future__ import unicode_literals
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^', include('travelers.urls')),
    # Admin
    url(r'^admin/', include(admin.site.urls)),
    # Social auth
    url('', include('social.apps.django_app.urls', namespace='social')),
)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
