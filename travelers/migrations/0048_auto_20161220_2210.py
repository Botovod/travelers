# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0047_city_show_on_mainmap'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sighphoto',
            name='posted',
            field=models.BooleanField(default=True, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='sighphoto',
            unique_together=set([('file', 'sight')]),
        ),
    ]
