# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0019_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='default_zoom',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u041c\u0430\u0441\u0448\u0442\u0430\u0431', blank=True),
            preserve_default=True,
        ),
    ]
