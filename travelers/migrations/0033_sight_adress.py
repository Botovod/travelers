# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0031_auto_20150812_1125'),
    ]

    operations = [
        migrations.AddField(
            model_name='sight',
            name='adress',
            field=models.CharField(default='', max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441', blank=True),
            preserve_default=True,
        ),
    ]
