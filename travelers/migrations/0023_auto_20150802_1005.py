# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0022_auto_20150802_1004'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='typeofsights',
            name='sights',
        ),
        migrations.AddField(
            model_name='sight',
            name='type',
            field=models.ManyToManyField(to='travelers.TypeOfSights', verbose_name='\u0414\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438'),
            preserve_default=True,
        ),
    ]
