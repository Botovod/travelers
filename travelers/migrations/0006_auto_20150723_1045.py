# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0005_auto_20150723_1028'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='link',
            field=models.CharField(default='', max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', db_index=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='travel',
            name='slug',
            field=models.SlugField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='travel',
            name='tag',
            field=models.CharField(default='', max_length=255, verbose_name='\u0422\u044d\u0433', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='traveler',
            name='link',
            field=models.CharField(default='', max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True),
            preserve_default=True,
        ),
    ]
