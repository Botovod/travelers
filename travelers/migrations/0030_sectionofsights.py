# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0029_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='SectionOfSights',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default='', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('type', models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0434\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438', blank=True, to='travelers.TypeOfSights', null=True)),
            ],
            options={
                'verbose_name': '\u0420\u0430\u0437\u0434\u0435\u043b \u0434\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0435\u0439',
                'verbose_name_plural': '\u0420\u0430\u0437\u0434\u0435\u043b\u044b \u0434\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0435\u0439',
            },
            bases=(models.Model,),
        ),
    ]
