# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0025_city_centre_coordinates'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='city',
            options={'verbose_name': '\u0413\u043e\u0440\u043e\u0434', 'verbose_name_plural': '\u0413\u043e\u0440\u043e\u0434\u0430'},
        ),
        migrations.AddField(
            model_name='city',
            name='regional_center',
            field=models.BooleanField(default=True, verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u044b\u0439 \u0446\u0435\u043d\u0442\u0440'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sight',
            name='regional',
            field=models.BooleanField(default=False, verbose_name='\u0412 \u0440\u0435\u0433\u0438\u043e\u043d\u0435'),
            preserve_default=True,
        ),
    ]
