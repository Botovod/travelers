# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0013_post_posted'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='sight',
            field=models.ForeignKey(verbose_name='\u0414\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c', blank=True, to='travelers.Sight', null=True),
            preserve_default=True,
        ),
    ]
