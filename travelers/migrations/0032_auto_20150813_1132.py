# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0031_auto_20150812_1125'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sight',
            name='add_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sight',
            name='fix_date',
            field=models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0441\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sight',
            name='price',
            field=models.CharField(max_length=100, null=True, verbose_name='\u0426\u0435\u043d\u0430', blank=True),
            preserve_default=True,
        ),
    ]
