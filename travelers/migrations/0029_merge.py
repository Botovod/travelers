# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0026_auto_20150802_2249'),
        ('travelers', '0028_auto_20150731_0925'),
    ]

    operations = [
    ]
