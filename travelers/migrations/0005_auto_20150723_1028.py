# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0004_auto_20150723_0949'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostPhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.ImageField(default='', upload_to='images/photo/', verbose_name='\u0424\u043e\u0442\u043e')),
                ('posts', models.ForeignKey(verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f', to='travelers.Post')),
            ],
            options={
                'verbose_name': '\u0424\u043e\u0442\u043e',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='post',
            name='photo',
        ),
        migrations.RemoveField(
            model_name='travel',
            name='posts',
        ),
        migrations.AddField(
            model_name='post',
            name='coordinates',
            field=models.CharField(default='', max_length=255, verbose_name='\u041a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u044b', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='link',
            field=models.CharField(default='', max_length=255, blank=True, unique=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='title',
            field=models.CharField(default='', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='travel',
            name='slug',
            field=models.SlugField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441', db_index=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='travel',
            name='tag',
            field=models.CharField(default='', unique=True, max_length=255, verbose_name='\u0422\u044d\u0433', blank=True),
            preserve_default=True,
        ),
    ]
