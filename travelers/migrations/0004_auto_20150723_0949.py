# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0003_auto_20150715_1350'),
    ]

    operations = [
        migrations.AddField(
            model_name='travel',
            name='city',
            field=models.CharField(default='', max_length=255, verbose_name='\u0413\u043e\u0440\u043e\u0434'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='travel',
            name='photo',
            field=models.CharField(default='', max_length=255, verbose_name='\u0424\u043e\u0442\u043e'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='travel',
            name='posts',
            field=models.ManyToManyField(to='travelers.Post', verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='traveler',
            name='sourses',
            field=models.ManyToManyField(to='travelers.Sourse', verbose_name='\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a\u0438', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='travel',
            name='sourses',
            field=models.ManyToManyField(to='travelers.Sourse', verbose_name='\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a\u0438'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='traveler',
            name='link',
            field=models.CharField(default='', unique=True, max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True),
            preserve_default=True,
        ),
    ]
