# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0024_sight_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='typeofsights',
            name='marker',
            field=models.CharField(default='accesdenied', max_length=30, verbose_name='\u041c\u0430\u0440\u043a\u0435\u0440'),
            preserve_default=True,
        ),
    ]
