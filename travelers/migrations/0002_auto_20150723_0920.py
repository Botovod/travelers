# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='traveler',
            name='link',
            field=models.CharField(default='', unique=True, max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='traveler',
            name='photo',
            field=models.CharField(default='http://gpsnew.ru/images/products/no_picture.jpg', max_length=255, verbose_name='\u0424\u043e\u0442\u043e'),
            preserve_default=True,
        ),
    ]
