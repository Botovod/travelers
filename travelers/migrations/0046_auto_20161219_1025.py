# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0045_region_description'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sight',
            old_name='adress',
            new_name='address',
        ),
    ]
