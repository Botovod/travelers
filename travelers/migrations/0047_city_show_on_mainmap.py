# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0046_auto_20161219_1025'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='show_on_mainmap',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0433\u0430\u0432\u043d\u043e\u0439 \u043a\u0430\u0440\u0442\u0435'),
            preserve_default=True,
        ),
    ]
