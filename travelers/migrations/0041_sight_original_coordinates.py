# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0040_city_region'),
    ]

    operations = [
        migrations.AddField(
            model_name='sight',
            name='original_coordinates',
            field=models.CharField(default='', max_length=100, verbose_name='\u0421\u0442\u044f\u043d\u0443\u0442\u043d\u044b\u0435 \u043a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u044b', blank=True),
            preserve_default=True,
        ),
    ]
