# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0036_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sectionofsights',
            name='type',
        ),
        migrations.AddField(
            model_name='sectionofsights',
            name='types',
            field=models.ManyToManyField(to='travelers.TypeOfSights', null=True, verbose_name='\u0422\u0438\u043f\u044b', blank=True),
            preserve_default=True,
        ),
    ]
