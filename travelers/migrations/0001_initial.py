# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default='', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('link', models.CharField(default='', unique=True, max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', db_index=True)),
                ('photo', models.CharField(default='', max_length=255, verbose_name='\u0424\u043e\u0442\u043e')),
                ('text', models.TextField(default='', verbose_name='\u0422\u0435\u043a\u0441\u0442')),
            ],
            options={
                'verbose_name': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sourse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.CharField(default='', unique=True, max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', db_index=True)),
            ],
            options={
                'verbose_name': '\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a',
                'verbose_name_plural': '\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Travel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default='', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('slug', models.SlugField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('tag', models.CharField(default='', unique=True, max_length=255, verbose_name='\u0422\u044d\u0433')),
                ('event_link', models.CharField(default='', unique=True, max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u043e\u0431\u044b\u0442\u0438\u0435')),
                ('posts', models.ManyToManyField(to='travelers.Post', verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f')),
                ('sourses', models.ManyToManyField(to='travelers.Sourse', verbose_name='\u0418\u0441\u043e\u0447\u043d\u0438\u043a\u0438')),
            ],
            options={
                'verbose_name': '\u041f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u0435',
                'verbose_name_plural': '\u041f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Traveler',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('link', models.CharField(default='', unique=True, max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430')),
                ('photo', models.CharField(default='', max_length=255, verbose_name='\u0424\u043e\u0442\u043e')),
            ],
            options={
                'verbose_name': '\u041f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0435\u043d\u043d\u0438\u043a',
                'verbose_name_plural': '\u041f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0435\u043d\u043d\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='travel',
            name='travelers',
            field=models.ManyToManyField(to='travelers.Traveler', verbose_name='\u041f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0435\u043d\u043d\u0438\u043a\u0438'),
            preserve_default=True,
        ),
    ]
