# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0032_auto_20150813_1132'),
        ('travelers', '0033_sight_adress'),
    ]

    operations = [
    ]
