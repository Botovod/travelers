# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0038_auto_20151124_1431'),
    ]

    operations = [
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default='', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0420\u0435\u0433\u0438\u043e\u043d',
                'verbose_name_plural': '\u0420\u0435\u0433\u0438\u043e\u043d\u044b',
            },
            bases=(models.Model,),
        ),
    ]
