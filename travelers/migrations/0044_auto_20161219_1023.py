# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0043_auto_20161010_2021'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='city',
            options={'ordering': ('title', 'slug'), 'verbose_name': '\u0413\u043e\u0440\u043e\u0434', 'verbose_name_plural': '\u0413\u043e\u0440\u043e\u0434\u0430'},
        ),
        migrations.AlterModelOptions(
            name='region',
            options={'ordering': ('title',), 'verbose_name': '\u0420\u0435\u0433\u0438\u043e\u043d', 'verbose_name_plural': '\u0420\u0435\u0433\u0438\u043e\u043d\u044b'},
        ),
        migrations.AlterField(
            model_name='city',
            name='description',
            field=models.TextField(default='', verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
    ]
