# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0021_auto_20150728_1037'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Type_of_sights',
            new_name='TypeOfSights',
        ),
    ]
