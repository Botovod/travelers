# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0020_city_default_zoom'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='posted',
            field=models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='route',
            name='posted',
            field=models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sighphoto',
            name='posted',
            field=models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sight',
            name='posted',
            field=models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='type_of_sights',
            name='colour',
            field=models.CharField(default='#0000FF', max_length=30, verbose_name='\u0426\u0432\u0435\u0442'),
            preserve_default=True,
        ),
    ]
