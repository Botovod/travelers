# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0024_sight_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='centre_coordinates',
            field=models.CharField(default='', max_length=100, verbose_name='\u041a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u044b', blank=True),
            preserve_default=True,
        ),
    ]
