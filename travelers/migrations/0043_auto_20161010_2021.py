# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0042_auto_20161008_1332'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='autotravel_url',
            field=models.CharField(default='', max_length=255, verbose_name='URL', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sight',
            name='autotravel_url',
            field=models.CharField(default='', max_length=255, verbose_name='URL', blank=True),
            preserve_default=True,
        ),
    ]
