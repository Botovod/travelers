# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0031_auto_20150812_1125'),
    ]

    operations = [
        migrations.AddField(
            model_name='route',
            name='city',
            field=models.ForeignKey (verbose_name='\u0413\u043e\u0440\u043e\u0434', to='travelers.City'),
            preserve_default=False,
        ),
    ]