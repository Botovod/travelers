# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0017_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(default='', max_length='50', verbose_name='\u0422\u044d\u0433')),
                ('post', models.ManyToManyField(to='travelers.Post', verbose_name='\u0417\u0430\u043f\u0438\u0441\u044c')),
            ],
            options={
                'verbose_name': '\u0422\u044d\u0433',
                'verbose_name_plural': '\u0422\u044d\u0433\u0438',
            },
            bases=(models.Model,),
        ),
    ]
