# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0013_post_posted'),
    ]

    operations = [
        migrations.CreateModel(
            name='Type_of_sights',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default='', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('time', models.CharField(default='', max_length=100, verbose_name='\u0412\u0440\u0435\u043c\u044f', blank=True)),
                ('colour', models.CharField(default='', max_length=30, verbose_name='\u0426\u0432\u0435\u0442')),
                ('sights', models.ManyToManyField(to='travelers.Sight', verbose_name='\u0414\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0434\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0434\u043e\u0441\u0442\u043e\u043f\u0440\u0438\u043c\u0435\u0447\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0435\u0439',
            },
            bases=(models.Model,),
        ),
    ]
