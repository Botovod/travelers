# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('travelers', '0011_auto_20150723_2314'),
    ]

    operations = [
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('places', models.ManyToManyField(to='travelers.Sight', verbose_name='\u041c\u0435\u0441\u0442\u0430')),
                ('user', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0440\u0448\u0440\u0443\u0442',
                'verbose_name_plural': '\u041c\u0430\u0440\u0448\u0440\u0443\u0442\u044b',
            },
            bases=(models.Model,),
        ),
    ]
