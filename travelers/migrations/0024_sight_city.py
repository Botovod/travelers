# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0023_auto_20150802_1005'),
    ]

    operations = [
        migrations.AddField(
            model_name='sight',
            name='city',
            field=models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', blank=True, to='travelers.City', null=True),
            preserve_default=True,
        ),
    ]
