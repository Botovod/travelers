# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0034_merge'),
        ('travelers', '0035_route_city'),
    ]

    operations = [
    ]
