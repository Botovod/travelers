# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0030_sectionofsights'),
    ]

    operations = [
        migrations.CreateModel(
            name='PopularPlace',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_with', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441')),
                ('time_to', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e')),
                ('sight', models.ForeignKey(default='', verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u044b\u0435 \u043c\u0435\u0441\u0442\u0430', to='travelers.Sight')),
            ],
            options={
                'verbose_name': '\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u043e\u0435 \u043c\u0435\u0441\u0442\u043e',
                'verbose_name_plural': '\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u044b\u0435 \u043c\u0435\u0441\u0442\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PopularTraces',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_with', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441')),
                ('time_to', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e')),
                ('sight', models.ForeignKey(default='', verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u044b\u0435 \u043c\u0435\u0441\u0442\u0430', to='travelers.Sight')),
            ],
            options={
                'verbose_name': '\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u044b\u0439 \u043c\u0430\u0440\u0448\u0440\u0443\u0442',
                'verbose_name_plural': '\u041f\u043e\u043f\u0443\u044f\u043b\u0440\u043d\u044b\u0435 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='sight',
            name='add_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 12, 11, 25, 9, 793771, tzinfo=utc), verbose_name='\u0414\u0430\u0442\u0430 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sight',
            name='fix_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 12, 11, 25, 20, 30711, tzinfo=utc), verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0441\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sight',
            name='price',
            field=models.CharField(default='null', max_length=100, verbose_name='\u0426\u0435\u043d\u0430', blank=True),
            preserve_default=True,
        ),
    ]
