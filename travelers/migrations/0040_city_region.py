# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0039_region'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='region',
            field=models.ForeignKey(default=None, verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', to='travelers.Region', null=True),
            preserve_default=True,
        ),
    ]
