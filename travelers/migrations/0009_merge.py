# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelers', '0007_merge'),
        ('travelers', '0008_auto_20150723_1012'),
    ]

    operations = [
    ]
