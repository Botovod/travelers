#coding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Sourse(models.Model):
    link = models.CharField('Ссылка', max_length=255, default='', blank=False, unique=True, db_index=True)

    class Meta:
        verbose_name = 'Источник'
        verbose_name_plural = 'Источники'


class TypeOfSights (models.Model):
    title = models.CharField('Название', max_length=255, default='', blank=False)
    time = models.CharField('Время', max_length=100, default='', blank=True)
    colour = models.CharField('Цвет', max_length=30, default='#0000FF', blank=False)
    marker = models.CharField('Маркер', max_length=30, default='marks/flag-export.png', blank=False)

    class Meta:
        verbose_name = 'Тип достопримечательности'
        verbose_name_plural = 'Типы достопримечательностей'


class SectionOfSights(models.Model):
    title = models.CharField('Название', max_length=255, default='', blank=False)
    types = models.ManyToManyField(TypeOfSights, verbose_name='Типы', blank=True, null=True)

    class Meta:
        verbose_name = 'Раздел достопримечательностей'
        verbose_name_plural = 'Разделы достопримечательностей'


class Region(models.Model):
    title = models.CharField('Название', max_length=255, default='', blank=False)
    description = models.TextField('Описание', default='', blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'
        ordering = ('title', )


class CityManager(models.Manager):
    def posted(self):
        return self.filter(posted=True)


class City(models.Model):
    title = models.CharField('Название', max_length=255, default='', blank=False)
    description = models.TextField('Описание', default='', blank=True)
    slug = models.SlugField('Адрес', max_length=255, blank=False, db_index=True)
    default_zoom = models.PositiveSmallIntegerField('Масштаб', blank=True, default=0)
    centre_coordinates = models.CharField('Координаты', max_length=100, default='', blank=True)
    posted = models.BooleanField('Опубликовано', default=False)
    show_on_mainmap = models.BooleanField('Показывать на гавной карте', default=False)
    regional_center = models.BooleanField('Региональный центр', default=True)
    region = models.ForeignKey(Region, verbose_name='Регион', null=True, blank=False, default=None)
    add_date = models.DateTimeField('Дата добавления', null=True, blank=False, auto_now_add=True)
    last_modified_date = models.DateTimeField('Дата последнего изменения', null=True, blank=False, auto_now=True)
    autotravel_url = models.CharField('URL', max_length=255, default='', blank=True)

    objects = CityManager()

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'
        ordering = ('title', 'slug')


class Sight (models.Model):
    title = models.CharField('Название', max_length=255, default='', blank=False)
    text = models.TextField('Описание', default='', blank=False)
    original_coordinates = models.CharField('Стянутные координаты', max_length=100, default='', blank=True)
    coordinates = models.CharField('Координаты', max_length=100, default='', blank=True)
    posted = models.BooleanField('Опубликовано', default=False)
    type = models.ManyToManyField(TypeOfSights, verbose_name='Достопримечательности', blank=False, null=False)
    city = models.ForeignKey(City, verbose_name='Город', blank=True, null=True)
    regional = models.BooleanField('В регионе', default=False)
    price = models.CharField('Цена', max_length=100, null=True, blank=True)
    add_date = models.DateTimeField('Дата добавления', null=True, blank=False, auto_now_add=True)
    fix_date = models.DateTimeField('Дата исправления', null=True, blank=False, auto_now=True)
    address = models.CharField('Адрес', max_length=255, default='', blank=True)
    autotravel_url = models.CharField('URL', max_length=255, default='', blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Достопримечательность'
        verbose_name_plural = 'Достопримечтельности'


class Traveler(models.Model):
    name = models.CharField('Имя', max_length=255, default='', blank=False)
    link = models.CharField('Ссылка', max_length=255, default='', blank=True, unique=True)
    photo = models.CharField('Фото', max_length=255, default='http://gpsnew.ru/images/products/no_picture.jpg', blank=False)
    sourses = models.ManyToManyField(Sourse, verbose_name='Источники', blank=False, null=False)

    class Meta:
        verbose_name = 'Путешественник'
        verbose_name_plural = 'Путешественники'


class Post(models.Model):
    title = models.CharField('Заголовок', max_length=255, default='', blank=True)
    link = models.CharField('Ссылка', max_length=255, default='', blank=True, db_index=True)
    text = models.TextField('Текст', default='', blank=False)
    coordinates = models.CharField('Координаты', max_length=100, default='', blank=True)
    posted = models.BooleanField('Опубликовано', default=False)
    sight = models.ForeignKey(Sight, verbose_name='Достопримечательность', blank=True, null=True)

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class PostPhoto(models.Model):
    file = models.ImageField('Изображение', default='', blank=False, upload_to='images/photo/')
    posts = models.ForeignKey(Post, verbose_name='Сообщения', blank=False, null=False)

    class Meta:
        verbose_name = 'Фотография отзыва'
        verbose_name_plural = 'Фотографии отзыва'


class Tag(models.Model):
    tag = models.CharField('Тэг', max_length="50", default='', blank=False, null=False)
    post = models.ManyToManyField(Post, verbose_name='Запись', blank=False, null=False)

    class Meta:
        verbose_name = 'Тэг'
        verbose_name_plural = 'Тэги'


class SighPhoto(models.Model):
    file = models.ImageField('Изображение', default='', blank=False, upload_to='images/photo/')
    sight = models.ForeignKey(Sight, verbose_name='Достпримечательность', blank=False, null=True)
    posted = models.BooleanField('Опубликовано', default=True)

    class Meta:
        verbose_name = 'Фотография достопримечательности'
        verbose_name_plural = 'Фотографии достопримечательности'
        unique_together = ('file', 'sight')


class Travel(models.Model):
    title = models.CharField('Название', max_length=255, default='', blank=False)
    slug = models.SlugField('Адрес', max_length=255, blank=False, db_index=True)
    tag = models.CharField('Тэг', max_length=255, default='', blank=True)
    travelers = models.ManyToManyField(Traveler, verbose_name='Путешественники', blank=False, null=False)
    sourses = models.ManyToManyField(Sourse, verbose_name='Источники', blank=False, null=False)
    photo = models.CharField('Фото', max_length=255, default='', blank=False)
    city = models.CharField('Город', max_length=255, default='', blank=False)

    class Meta:
        verbose_name = 'Путешествие'
        verbose_name_plural = 'Путешествия'


class Route(models.Model):
    places = models.ManyToManyField(Sight, verbose_name='Места', blank=False, null=False)
    user = models.ForeignKey(User, verbose_name='Пользователь', blank=True, null=True)
    posted = models.BooleanField('Опубликовано', default=False)
    city = models.ForeignKey(City, verbose_name='Город', blank=False, null=False)

    class Meta:
        verbose_name = 'Маршрут'
        verbose_name_plural = 'Маршруты'


class PopularPlace(models.Model):
    sight = models.ForeignKey(Sight, verbose_name='Популярные места', blank=False, default='')
    time_with = models.DateTimeField('Время с', auto_now_add=True, )
    time_to = models.DateTimeField('Время до', auto_now_add=True, )

    class Meta:
        verbose_name = 'Популярное место'
        verbose_name_plural = 'Популярные места'


class PopularTraces(models.Model):
    sight = models.ForeignKey(Sight, verbose_name='Популярные места', blank=False, default='')
    time_with = models.DateTimeField('Время с', auto_now_add=True,  )
    time_to = models.DateTimeField('Время до', auto_now_add=True, )

    class Meta:
        verbose_name = 'Популярный маршрут'
        verbose_name_plural = 'Попуялрные маршруты'
