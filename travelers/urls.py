#coding: utf-8
from __future__ import unicode_literals
from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('travelers.views.sight',
    url(r'^sight/(?P<pk>\d+)/$', 'sight', name='Sight'),
)

urlpatterns += patterns('travelers.views.trace',
    url(r'^city/(?P<city_slug>[-_\w]+)/traces/$', 'city_traces', name='CityTraces'),
)

urlpatterns += patterns('travelers.views.travel',
    url(r'^travels/$', 'all_travels', name='Travels'),
    url(r'^travels/(?P<travel_slug>[-_\w]+)/map/$', 'travel_map', name='TravelMap'),
)

urlpatterns += patterns('travelers.views.traveller',
    url(r'^travelers/$', 'all_travelers', name='Travelers'),
)

urlpatterns += patterns('travelers.views.blog',
    url(r'^blog/$', 'main_blog', name='MainBlog'),
    url(r'^travels/(?P<travel_slug>[-_\w]+)/blog/$', 'travel_blog', name='TravelBlog'),
)

urlpatterns += patterns('travelers.views.city',
    url(r'^$', 'cities', name='Home'),
    url(r'^map/$', 'main_map', name='MainMap'),
    url(r'^city/(?P<city_slug>[-_\w]+)/main/$', 'selected_city', name='MainCity'),
    url(r'^city/(?P<city_slug>[-_\w]+)/map/$', 'city_map', name='CityMap'),
    url(r'^city/(?P<city_slug>[-_\w]+)/sights/$', 'city_sights', name='CitySights'),
    url(r'^cities/$', 'cities', name='Cities'),
    url(r'^region/(?P<pk>\d+)/$', 'region', name='Region'),
)

urlpatterns += patterns('travelers.views.region',
    url(r'^travelers/$', 'all_regions', name='Regions'),
)

urlpatterns += patterns('travelers.views.auth',
    url(r'^logout/$', 'logout'),
)
