# coding: utf-8
from __future__ import unicode_literals
from travelers.models import Traveler, Travel, Post, Sourse, Sight, PostPhoto, Route, TypeOfSights, SectionOfSights, SighPhoto, City
from django.contrib import admin


class NoCoordinatesListFilter(admin.SimpleListFilter):

    title = 'Без координат'

    parameter_name = 'coordinates'

    def lookups(self, request, model_admin):
        return (
            ('No', 'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'No':
            return queryset.filter(centre_coordinates='')
        return queryset


class SightAdmin(admin.ModelAdmin):
    list_filter = ('posted', 'coordinates')


class CityAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'posted', 'show_on_mainmap', 'regional_center')
    list_editable = ('posted', 'show_on_mainmap', 'regional_center')
    list_filter = ('posted', 'regional_center', 'show_on_mainmap', 'region', NoCoordinatesListFilter)
    search_fields = ('title', )


admin.site.register(Traveler)
admin.site.register(Travel)
admin.site.register(Post)
admin.site.register(City, CityAdmin)
admin.site.register(Sourse)
admin.site.register(Sight, SightAdmin)
admin.site.register(PostPhoto)
admin.site.register(SighPhoto)
admin.site.register(Route)
admin.site.register(TypeOfSights)
admin.site.register(SectionOfSights)

SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['username', 'first_name', 'email']
SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['field1', 'field2']
