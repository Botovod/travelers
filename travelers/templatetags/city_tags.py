# coding: utf-8
from __future__ import unicode_literals
from travelers.models import City
from django.template.base import Library

register = Library()


@register.inclusion_tag('templatetags/city_chooser.html', takes_context=True)
def city_chooser(context):
    context.update({"cities": City.objects.filter(posted=True)})
    return context


@register.inclusion_tag('templatetags/current_city.html', takes_context=True)
def current_city(context):
    try:
        current_city = City.objects.get(slug=context['request'].get_full_path().split('/')[2])
    except (City.DoesNotExist, IndexError):
        pass
    else:
        context.update({"current_city": current_city})
    return context
