# coding: utf-8
from __future__ import unicode_literals
from django.template.base import Library
from travelers.models import SectionOfSights, City, Region

register = Library()


@register.inclusion_tag('templatetags/places_menu.html', takes_context=True)
def places_menu(context):
    context.update({'sections': SectionOfSights.objects.all()})
    return context


@register.inclusion_tag('templatetags/cities_menu.html', takes_context=True)
def cities_menu(context):
    context.update({'regions': Region.objects.all()})
    return context


@register.inclusion_tag('templatetags/cities_menu.html', takes_context=True)
def region_menu(context):
    context.update({'regions': [context['region'], ]})
    return context
