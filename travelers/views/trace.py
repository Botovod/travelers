# coding: utf-8
from __future__ import unicode_literals
from django.views.generic import ListView
from travelers.models import Route
from travelers.views.mixins import CityMixin


class CityTraces(CityMixin, ListView):
    template_name = 'maintraces.html'
    model = Route
    context_object_name = 'traces'
city_traces = CityTraces.as_view()
