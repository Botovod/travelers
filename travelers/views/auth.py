# coding: utf-8
from __future__ import unicode_literals
from django.contrib.auth import login
from social.apps.django_app.utils import load_strategy, load_backend
from social.exceptions import AuthTokenRevoked
from django.shortcuts import render, redirect
from django.contrib.auth import logout as auth_logout


def home(request):
    if 'access_token' in request.GET:
        backend = 'vk-oauth2'
        request.social_strategy = load_strategy(request)
        uri = '/'

        request.backend = load_backend(request.social_strategy, backend, uri)

        token = request.GET['access_token']
        try:
            user = request.backend.do_auth(token)
        except AuthTokenRevoked:
            user = None
        else:
            if user:
                login(request, user)
                print('OK')
            else:
                print('ERROR')
            return redirect('/')
    return render(request, 'base.html')


def logout(request):
    auth_logout(request)
    return redirect('/')
