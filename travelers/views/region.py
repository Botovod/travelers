# coding: utf-8
from __future__ import unicode_literals
from django.views.generic import ListView
from travelers.models import Region


class AllRegions(ListView):
    model = Region
all_regions = AllRegions.as_view()
