# coding: utf-8
from __future__ import unicode_literals
from django.views.generic import ListView, DetailView
from travelers.views.mixins import PostedMixin, CityMixin
from travelers.models import City, Sight, Region


class Map(PostedMixin, ListView):
    template_name = 'mainmap.html'
    model = Sight
    context_object_name = 'sights'
main_map = Map.as_view()


class CityMap(CityMixin, Map):
    template_name = 'citymap.html'

    def get_queryset(self):
        return super(CityMap, self).get_queryset().exclude(coordinates='')
city_map = CityMap.as_view()


class SelectedCity(DetailView):
    template_name = 'city.html'
    model = City
    context_object_name = 'city'
    slug_url_kwarg = 'city_slug'
selected_city = SelectedCity.as_view()


class CitySights(CityMap):
    template_name = 'CitySights.html'
city_sights = CitySights.as_view()


class MainCity(PostedMixin, ListView):
    template_name = 'cities.html'
    model = City
    context_object_name = 'cities'

    def get_queryset(self):
        return super(MainCity, self).get_queryset().filter(show_on_mainmap=True)
cities = MainCity.as_view()


class RegionalMap(DetailView):
    template_name = 'region.html'
    model = Region
    context_object_name = 'region'
region = RegionalMap.as_view()
