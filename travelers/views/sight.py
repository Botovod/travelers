# coding: utf-8
from __future__ import unicode_literals
from django.views.generic import DetailView
from travelers.models import Sight


class SightView(DetailView):
    template_name = 'sight.html'
    model = Sight
    context_object_name = 'sight'
sight = SightView.as_view()
