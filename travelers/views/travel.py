# coding: utf-8
from __future__ import unicode_literals
from django.views.generic import ListView
from travelers.models import Travel, Post
from travelers.views.mixins import TravelMixin


class AllTravels(ListView):
    model = Travel
    template_name = 'maintravels.html'
    context_object_name = 'travels'
all_travels = AllTravels.as_view()


class TravelMap(TravelMixin, ListView):
    model = Post
travel_map = TravelMap.as_view()
