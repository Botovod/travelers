# coding: utf-8
from __future__ import unicode_literals
from travelers.models import City


class PostedMixin(object):
    def get_queryset(self):
        return super(PostedMixin, self).get_queryset().filter(posted=True)


class TravelMixin(object):
    template_context_name = 'travel_posts'

    def get_queryset(self):
        return super(TravelMixin, self).get_queryset().filter(slug=self.kwargs['travel_slug'])


class CityMixin(object):
    template_context_name = 'travel_posts'

    def get_queryset(self):
        return super(CityMixin, self).get_queryset().filter(city__slug=self.kwargs['city_slug'])

    def get_context_data(self, **kwargs):
        context = super(CityMixin, self).get_context_data(**kwargs)
        context['selected_city'] = City.objects.get(slug=self.kwargs['city_slug'])
        return context
