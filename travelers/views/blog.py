# coding: utf-8
from __future__ import unicode_literals
from django.views.generic import ListView
from travelers.views.mixins import PostedMixin, TravelMixin
from travelers.models import Post


class MainBlog(PostedMixin, ListView):
    model = Post
    template_name = 'mainblog.html'
    context_object_name = 'travel_reviews'
main_blog = MainBlog.as_view()


class TravelBlog(TravelMixin, MainBlog):
    template_name = 'travelblog.html'
travel_blog = TravelBlog.as_view()
