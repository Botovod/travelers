# coding: utf-8
from __future__ import unicode_literals
from django.views.generic import ListView
from travelers.models import Traveler


class AllTravelers(ListView):
    model = Traveler
    template_name = 'maintravelers.html'
    context_object_name = 'travelers'
all_travelers = AllTravelers.as_view()
